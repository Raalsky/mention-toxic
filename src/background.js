// import 'babel-polyfill';
import * as tf from '@tensorflow/tfjs';
import * as toxicity from '@tensorflow-models/toxicity';

let model = toxicity.load(0.75)

chrome.runtime.onConnect.addListener(function(port) {
  console.assert(port.name == "toxic-classifier");
  port.onMessage.addListener(
    function(msg) {
      model.then(model => {
        console.log(msg.texts);
        model.classify(msg.texts).then((predictions) => {
          port.postMessage({results: predictions});
        });
      });
      return true;
    });
});
