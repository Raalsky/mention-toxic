function capitalizeFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

const processPredictionClass = (prediction_class) => {
  var results = Array()
  prediction_class.results.forEach(function(result) {
    if (result.match == true) {
      results.push({
        'label': prediction_class.label,
        'score': result.probabilities[1]
      });
    }
    else {
      results.push({});
    }
  });
  return results;
};

const getTop3 = (predictions) => {
  return predictions.sort(function(a, b) {
      if (a.score > b.score) return -1;
      if (a.score < b.score) return 1;
    return 0;
  }).slice(0, 3)
}

const processResponse = (predictions) => {
  var res = Array()
  predictions.forEach(function(prediction_class) {
    res.push(processPredictionClass(prediction_class));
  });
  var h = Array(res[0].length)
  for (const index of Array(res[0].length).keys()) {
    h[index] = Array()
    res.forEach(function(j){
      if (Object.keys(j[index]).length > 0) {
        h[index].push(j[index])
      }
    });
  }
  return h
}

const updateMentions = (response) => {
  results = processResponse(response.results);
  mentions = document.querySelectorAll('.mention')

  results.forEach(function(result, index) {
    classes = getTop3(result);
    header = mentions[index].querySelector('.mention-information')

    toxic_block = document.createElement("li");
    toxic_block.className = "toxicity"
    toxic = document.createElement("ul");

    if (classes.length == 0) {
      non_toxic = document.createElement("li");
      non_toxic.className = "non-toxic";
      non_toxic.innerHTML = "Non-Toxic";
      toxic.appendChild(non_toxic);
    }
    else {
      classes.forEach(function(toxicityClass) {
        current_toxic = document.createElement("li");
        var label = capitalizeFirst(toxicityClass.label);
        if (label == "Toxicity") {
          label = "Toxic"
        }
        current_toxic.innerHTML = label;
        toxic.appendChild(current_toxic);
      });
    }

    toxic_block.appendChild(toxic);
    header.insertBefore(toxic_block, mentions[index].querySelector('.sentiment').parentNode.nextSibling);
  });
}

const predictToxicity = (port, texts) => {
  port.postMessage({texts: texts});
}

const updateMentionsToxicity = (port) => {
  var texts = Array();
  document.querySelectorAll('.mention').forEach(function(el) {
    texts.push(el.querySelector('.mention_text').textContent)
  });
  predictToxicity(port, texts);
}

const main = () => {
  var port = chrome.runtime.connect({name: "toxic-classifier"});
  var resultsContent = document.getElementById('results_content');

  updateMentionsToxicity(port);

  const config = { attributes: false, childList: true, subtree: false };
  const observer = new MutationObserver(function(mutationsList, observer) {
    updateMentionsToxicity(port);
  });

  port.onMessage.addListener(updateMentions);
  observer.observe(resultsContent, config);
};

window.addEventListener('load', main, false);
